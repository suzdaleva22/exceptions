
public class Age {
    public static void checkAge () {
        try {
            // Перевіряємо вік користувача
            verifyAge(16);
        } catch (AgeException e) {
            // Обробляємо виняткове виключення, коли вік менше 18 років
            System.out.println("Помилка: " + e.getMessage());
        } catch (ArithmeticException e) {
            System.out.println("ArithmeticException" + e.getMessage());
        } catch (NullPointerException e) {
            System.out.println("NullPointerException: " + e.getMessage());
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("ArrayIndexOutOfBoundsException: " + e.getMessage());
        }catch (Exception e) {
            // Інші виключення
            System.out.println("Інше виняткове виключення: " + e.getMessage());
        } finally {
            System.out.println("Finally block is executed");
        }
    }

    // Метод для перевірки віку користувача
    public static void verifyAge(int age) throws AgeException {
        if (age < 18) {
            // Генеруємо власне виняткове виключення
            throw new AgeException("Вік користувача менше 18 років");
        } else {
            System.out.println("Вік користувача: " + age + " років");
        }
    }
}

class AgeException extends Exception {
    AgeException(String message) {
        super(message);
    }
}